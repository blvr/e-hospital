import { NgModule } from '@angular/core';
import { IonicModule } from 'ionic-angular';

/* Importing Providers */
import {
  CallHandler,
} from './providers';

/* Importing Classes Interfaces */
import {


} from './classes';

/* Exporting  Classes, Providers, Interfaces the "regular" way */
export {
  // providers
  CallHandler,
  // classes
};

@NgModule({
  providers: [ /* Don't add the services here */ ]
})

/* Exporting  Module with Singleton Providers */

export class ApiLibraryModule {
  static forRoot() {
    return {
      ngModule: ApiLibraryModule,
      providers: [
        CallHandler,
       ]
    };
  }
}
