import { NgModule } from '@angular/core';
import { HttpModule } from '@angular/http';

// Import custom providers
import { CallHandler } from './modules/callhandler/callhandler.provider';

// Export all services
export * from './modules/callhandler/callhandler.provider';

// Export convenience property
export {
  CallHandler
};