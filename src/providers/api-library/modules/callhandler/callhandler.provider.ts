import { Injectable } from '@angular/core';
import { Headers, Http, Request, RequestOptions, RequestOptionsArgs, URLSearchParams } from '@angular/http';
import * as _ from 'lodash';


@Injectable()
export class CallHandler {
  accessToken: string;
  serverURI: string = '';
  user: any = {};
  clientId = '12345';
  clientSecret = "secret";

  constructor(private http: Http) {
    this.serverURI = '';
    this.accessToken = '';
  };

  sendRequest(method: string, endpoint: string, payload: any) {
    let promise = new Promise((resolve, reject) => {
      /* determine token */

      /* check payload */

      /* append headers */
      let headers = new Headers();
      headers.append('Content-Type', 'application/x-www-form-urlencoded');
      if (this.accessToken) {
        headers.append('Authorization', 'Bearer ' + this.accessToken);
      }

      /* turn payload to URLSearchParams for Get type of call */
      let params: URLSearchParams = new URLSearchParams();
      if (method === 'GET') {
        for (let key in payload) {
          params.set(key, payload[key]);
        }
      }

      /* set requestoptions */
      let basicOptions: RequestOptionsArgs = {
        url: `${this.serverURI}${endpoint}`,
        method: method,
        search: params,
        headers: headers, // new Headers({'trustme':true}),
        body: payload
      };

      /* create actual request */
      let req: any = new Request(new RequestOptions(basicOptions));
      if (method === 'GET') {
        req.params = payload;
      } else {
        req.data = payload;
      }

      this.http.request(req)
        .map(res => res.json())
        .toPromise()
        .then((response) => {
          if (response.errors) {
            reject({ msg: 'error in callhandler\sendrequest.', err: response.errors });
          }
          else {
            resolve(response.data);
          }
        })
        .catch((err) => {
          reject({ msg: 'error in callhandler\sendrequest.', err: err });
        })
    });
    return promise;
  }

  anonymousLogin() {
    return new Promise((resolve, reject) => {
      this.http.post(
        this.serverURI + '/oauth/token/anonymous', {
          client_id: this.clientId,
          client_secret: this.clientSecret
          // name: "Kampitakis"
        })
        .map(res => res.json())
        .toPromise()
        .then((response) => {
          if (response.errors) {
            reject({ msg: 'error in anonymous log in.', err: response.errors });
          } else {
            this.accessToken = response.token
            resolve(this.accessToken);
          }
        }, (err) => {
          reject({ msg: 'error in anonymous log in.', err: err });
        })
    });
  }

  login(payload: any) {
    return new Promise((resolve, reject) => {
      // this.user = payload;
      let loginHeaders = new Headers();
      loginHeaders.append('Content-Type', 'application/x-www-form-urlencoded');
      this.http.post(this.serverURI + '/oauth/token', this.user, { headers: loginHeaders })
        .map(res => res.json())
        .toPromise()
        .then((response: any) => {
          if (response.errors) {
            reject({ msg: 'error while logging in', err: response.errors });
          } else {
            this.accessToken = response.token;
            resolve(response);
          }
        }, (err) => {
          reject({ msg: 'error while logging in', err: err });
        });
    });
  }
  logout() {
    let ret;
    ret = this.anonymousLogin();
    ret.then((token)=>{
      this.user = {};
    })
    .catch(err=>{
      console.log({ msg: 'error while logging out.', err: err });
    })

  }
}