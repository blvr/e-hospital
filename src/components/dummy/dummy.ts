import { Component } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';

/**
 * Generated class for the DummyPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@Component({
  selector: 'page-dummy',
  templateUrl: 'dummy.html',
})
export class DummyPage {

  dummyData: any;
  constructor(public navCtrl: NavController, public navParams: NavParams) {
    this.dummyData = this.navParams.get('dummyData');
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad DummyPage');
  }

}
