import { NgModule } from '@angular/core';
import { IonicModule } from 'ionic-angular';
import { DummyPage } from './dummy/dummy';
import { InformationPage } from './information/information';
import { MapPage } from './map/map';

// import { TranslateModule } from 'ng2-translate';

@NgModule({
  entryComponents: [
    DummyPage,
    InformationPage,
    MapPage
  ],
  declarations: [
    DummyPage,
    InformationPage,
    MapPage
  ],
  imports: [
    IonicModule,
    // IonicModule.forRoot(MenuItemComponent), This was suggested, but it's better to use the previous import so that ionic directives are available in all custom components
    // TranslateModule,
  ],
  exports: [
    DummyPage,
    InformationPage,
    MapPage
  ]
})
export class ComponentsModule { }

