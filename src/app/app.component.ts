import { Component, ViewChild } from '@angular/core';

import { Platform, MenuController, Nav } from 'ionic-angular';

import { HelloIonicPage } from '../pages/hello-ionic/hello-ionic';
import { ListPage } from '../pages/list/list';

import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';
import { Http } from '@angular/http';
import { TranslateService } from 'ng2-translate';
import { configuration } from '../env';
import { CallHandler } from '../providers/api-library/index';

@Component({
  templateUrl: 'app.html'
})
export class MyApp {
  @ViewChild(Nav) nav: Nav;

  // make HelloIonicPage the root (or first) page
  rootPage = HelloIonicPage;
  pages: Array<{ title: string, component: any }>;

  constructor(
    public platform: Platform,
    public menu: MenuController,
    public statusBar: StatusBar,
    public splashScreen: SplashScreen,
    public http: Http,
    private translate: TranslateService
    , private api: CallHandler
  ) {
    this.initializeApp();

    // set our app's pages
    this.pages = [
      { title: 'Login', component: 'LoginPage' }, // TODO remove
      { title: 'Home', component: 'HomePage' },
      { title: 'Profile', component: 'ProfilePage' },
      { title: 'Services', component: 'ServicesPage' },
      { title: 'Department', component: 'DepartmentPage' },
      { title: 'Appointments', component: 'AppointmentsPage' },
      { title: 'Logout', component: 'LogoutPage' },
      { title: 'List-Detail Example', component: ListPage },
      { title: 'TabsPage', component: 'TabsPage' }


      // { title: 'My First List', component: ListPage }
    ];
  }

  initializeApp() {
    this.platform.ready().then(() => {
      // Okay, so the platform is ready and our plugins are available.
      // Here you can do any higher level native things you might need.

      /* initialize actions */

      // get config
      let config: any = configuration;
      // get language from config or default en-US
      this.translate.setDefaultLang(config.language ? config.language : 'en-Us');
      // this.translate.setDefaultLang('en-US');
      this.translate.use(this.translate.getDefaultLang());
      /* end of initialize actions */
      this.api.serverURI = config.serverURI ? config.serverUri : 'http://128.199.52.73:4200'

      this.statusBar.styleDefault();
      this.splashScreen.hide();
    });
  }

  openPage(page) {
    // close the menu when clicking a link from the menu
    this.menu.close();
    // navigate to the new page if it is not the current page
    this.nav.setRoot(page.component);
  }
}
