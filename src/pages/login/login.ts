import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ToastController } from 'ionic-angular';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { TranslateService } from 'ng2-translate';
import { CallHandler } from '../../providers/api-library/index';
/**
 * Generated class for the LoginPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-login',
  templateUrl: 'login.html',
})
export class LoginPage {

  loginForm: FormGroup
  constructor(public navCtrl: NavController
    , public navParams: NavParams
    , private formBuilder: FormBuilder
    , private toastCtrl: ToastController
    , private translate: TranslateService
    , private api: CallHandler
  ) {
    this.loginForm = this.formBuilder.group({
      username: ['', [Validators.required /*, Validators.email*/]],
      password: ['', Validators.required],
    });
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad LoginPage');
  }

  login() {
    if (!this.loginForm.valid) {
      this.presentToast(this.translate.instant('LOGIN.ERRORS.CREDENTIALS'));
    }
    console.log('mocking up login...');
  }

  register() {
    console.log('mocking up registration...');
    this.api.anonymousLogin()
    .then(data=>{
      console.log('response from anonymous call', data);
    })
    .catch(err=>{
      console.log('error at anoynmous call', err);
    })
  }

  presentToast(message) {
    let toast = this.toastCtrl.create({
      message: message,
      duration: 3000,
      position: 'bottom'
    });

    toast.onDidDismiss(() => {
      console.log('Dismissed toast');
    });

    toast.present();
  }

}
