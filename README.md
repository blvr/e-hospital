
## Frontend Ionic app for the e-hospital student challenge project


### Clone
  - git clone git@bitbucket.org:blvr/e-hospital.git
  - git clone https://blvr@bitbucket.org/blvr/e-hospital.git


### Run
  1. npm install
  2. ionic serve

### Keep in mind
  1. When you install a package that you believe we are going to use, do not forget the --save-exact during npm install, example:
    `npm install --save-exact awesome-library `


## Contributors
  1. 
  2. 
  3. 
  4. 
 ... 
 100. 