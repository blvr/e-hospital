/* Tool that compares engish and italain translation.
 * A new json is created under utilities/ that has tagged inside
 * the missing values (_MISSING_) along with the english ones.
 * 
 * If show-common param is provided _SAME_ tag is added to common translations
 * in order to spot an unwanted, transferred translation.
 */

// TODO abstractify jsons, take them as an input, along with the output

'use strict';
var _ = require('lodash')
const fs = require('fs');
const args = process.argv;
// console.log(args[2]);
let en, it, final, mode, checkLanguage;
// if(args[2] && args[2] === 'show-common') {
//   mode = true;
// }
// else {
//   mode = false;
// }
if (args[2]) {

  checkLanguage = args[2];

  fs.readFile('src/assets/i18n/en-US.json', (err, data1) => {
    if (err) throw err;
    en = JSON.parse(data1);
    // console.log(en);
    fs.readFile(`src/assets/i18n/${checkLanguage}.json`, (err, data2) => {
      if (err) {
        console.log(`Error while accessing src/assets/i18n/${checkLanguage}.json`);
      }
      else {
        it = JSON.parse(data2);
        // console.log(en);
        final = {};
        applyData(en, '', mode);
        delete final[''];
        let datafinal = JSON.stringify(final, null, 4);
        fs.writeFileSync('utilities/missing.json', datafinal);
        console.log('Changes can be found in utilities/missing.json, missing values will have a _MISSING_ annotation.');
        // localStorage.setItem('newItalian', JSON.stringify(final));
        // // console.log(JSON.stringify(final));
      }
    });

  });

  // // console.log('This is after the read call');  

  function applyData(obj, _path, mode) {
    for (let key in obj) {
      // reset the path for each same-level key
      let path = _path;
      // if (!path) path = key;
      // console.log(path);
      // prepare the field in config if it not exist
      if (!get(final, path)) {
        set(path, {});
      }
      else {
        // console.log(`exists, ${path}['${key}']: `, get(final, `${path}['${key}']`));
      }
      if (obj[key] !== Object(obj[key])) {
        // console.log('we have a primitive type or array', obj[key], `path: config${path}['${key}']`);
        let result = get(it, path + `['${key}']`);
        if (!result) {
          set(`${path}['${key}']`, '_MISSING__' + obj[key]);
        }
        else if (result === obj[key] && mode) {
          set(`${path}['${key}']`, obj[key] + '_SAME_');
        }
        else {
          set(`${path}['${key}']`, result);
        }
      }
      // we have an object, apply above recursively /
      else {
        // console.log('we have an object', obj[key], `path: ${path}['${key}']`);
        applyData(obj[key], path += `['${key}']`, mode);
      }
    }
  }

  function get(config, key) {
    return _.get(config, key);
  }

  function set(key, value) {
    _.set(final, key, value);
    // storeConfig();
  }
}
else {
  console.log('Error while running the command, example:\nnpm run check-language el-GR');
}